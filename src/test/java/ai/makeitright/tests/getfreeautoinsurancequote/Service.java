package ai.makeitright.tests.getfreeautoinsurancequote;

import ai.makeitright.pages.AutoInsurance;
import ai.makeitright.pages.CustomerInformationPage;
import ai.makeitright.pages.VehiclePage;
import ai.makeitright.utilities.drivermanager.DriverConfig;
import ai.makeitright.base.Main;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Service extends DriverConfig {
    private String URL;
    private String ZIPCODE;

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("inputParameters.url","https://www.geico.com/auto-insurance/");
        System.setProperty("inputParameters.zipcode","90401");
        Main.serviceName = "Get free Auto Insurance Quote";
    }

    @Before
    public void before() {
        URL = System.getProperty("inputParameters.url");
        ZIPCODE = System.getProperty("inputParameters.zipcode");
    }

    @Test
    public void getFreeAutoInsuranceQuote() {
        driver.get(URL);
        driver.manage().window().maximize();
        AutoInsurance autoInsurance = new AutoInsurance(driver,URL);
        autoInsurance.setZIPCode(ZIPCODE);
        CustomerInformationPage customerInformationPage = autoInsurance.clickButtonReviewYourQuote();

        customerInformationPage.clickButtonNext_toStartFillCustomersInformation();

        customerInformationPage.setDateOfBirth("07","07","1991");
        customerInformationPage.clickButtonNext();

        customerInformationPage.setFirstName("Bil");
        customerInformationPage.setLastName("Cosby");
        customerInformationPage.clickButtonNext();

        customerInformationPage.setAddress("1717 4th Street");
        customerInformationPage.setApt("250");
        customerInformationPage.clickButtonNext();


        VehiclePage vehiclePage = customerInformationPage.clickButtonNo();

        vehiclePage.selectYear("2006");
        vehiclePage.selectMake("Toyota");
        vehiclePage.selectModel("Corolla");
        vehiclePage.clickButtonNext();

        vehiclePage.selectAntiTheftDevice("Alarm Only Or Active Disabling Device");
        vehiclePage.clickButtonNext();

        vehiclePage.clickButtonYes();

        vehiclePage.selectOwnershipStatus("Owned");

        vehiclePage.selectMainlyUse("Pleasure");

        vehiclePage.selectAnnualMileage("11,001 - 12,000");
        vehiclePage.clickButtonNext();

    }

}
