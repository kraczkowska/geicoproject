package ai.makeitright.pages;

import ai.makeitright.base.Main;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CustomerInformationPage extends BasePage {
    public CustomerInformationPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected boolean isAt() {
        Main.report.logInfo("Current URL address: " + driver.getCurrentUrl());
        Assert.assertEquals("Current URL address '" + driver.getCurrentUrl() + "' is not like expected 'https://auto-buy.geico.com/nb#/sale/customerinformation/gskmsi?id='","https://auto-buy.geico.com/nb#/sale/customerinformation/gskmsi?id=",driver.getCurrentUrl());
        return true;
    }

    @FindBy(xpath = "//*[@id='question-breakdown']//button[@type='button']")
    private WebElement btnNEXT_toStartFillCustomersInformation;

    @FindBy(xpath = "//div[@class='pull-right']/button")
    private WebElement btnNEXT;

    @FindBy(xpath = "//label[@for='hasMovedInLast2Months1']")
    private WebElement btnNo;

    @FindBy(id = "street")
    private WebElement inpAddress;

    @FindBy(id = "apt")
    private WebElement inpApt;

    @FindBy(xpath = "//input[@id='date-daydob']")
    private WebElement inpDD;

    @FindBy(id = "firstName")
    private WebElement inpFirstName;

    @FindBy(id = "lastName")
    private WebElement inpLastName;

    @FindBy(xpath = "//input[@id='date-monthdob']")
    private WebElement inpMM;

    @FindBy(xpath = "//input[@id='date-yeardob']")
    private WebElement inpYYYY;

    public void clickButtonNext_toStartFillCustomersInformation() {
        waitForClickable(btnNEXT_toStartFillCustomersInformation);
        click(btnNEXT_toStartFillCustomersInformation, "button 'NEXT'");
    }

    public void clickButtonNext() {
        click(btnNEXT, "button 'NEXT'");
    }

    public VehiclePage clickButtonNo() {
        waitForLoadingCircleDisappear();
        waitForClickable(btnNo);
        click(btnNo, "button 'No'");
        return new VehiclePage(driver);
    }

    public void setAddress(String address) {
        waitForVisibilityOf(inpAddress);
        sendText(inpAddress,address,"input element 'Address'");
    }

    public void setApt(String apt) {
        sendText(inpApt,apt,"input element 'Apt #'");
    }

    public void setDateOfBirth(String month, String day, String year) {
        waitForVisibilityOf(inpMM);
        sendText(inpMM,month,"input element 'MM' for Date of Birth");
        sendText(inpDD,day,"input element 'DD' for Date of Birth");
        sendText(inpYYYY,year,"input element 'YYYY' for Date of Birth");
    }

    public void setFirstName(String firstName) {
        waitForVisibilityOf(inpFirstName);
        sendText(inpFirstName,firstName,"input element 'First Name'");
    }

    public void setLastName(String lastName) {
        sendText(inpLastName,lastName,"input element 'Last Name'");
    }
}
