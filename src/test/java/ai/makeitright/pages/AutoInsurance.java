package ai.makeitright.pages;

import ai.makeitright.base.Main;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AutoInsurance extends BasePage {
    public AutoInsurance(WebDriver driver, String url) {
        super(driver, url);
    }

    @Override
    protected boolean isAt() {
        Main.report.logInfo("Current URL address: "+driver.getCurrentUrl());
        Assert.assertEquals("Current URL address '" + driver.getCurrentUrl() + "' is not like expected '" + url + "'",url,driver.getCurrentUrl());
        return true;
    }

    @FindBy(id = "submit")
    private WebElement btnReviewYourQuote;

    @FindBy(id = "zip")
    private WebElement inpZIPCode;


    public void setZIPCode(String ZIPCode) {
        sendText(inpZIPCode,ZIPCode,"input element 'ZIPCode'");
    }

    public CustomerInformationPage clickButtonReviewYourQuote() {
        click(btnReviewYourQuote,"button 'Review Your Quote'");
        return new CustomerInformationPage(driver);
    }
}
