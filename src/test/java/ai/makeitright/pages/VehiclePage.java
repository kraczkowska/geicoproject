package ai.makeitright.pages;

import ai.makeitright.base.Main;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class VehiclePage extends BasePage {
    public VehiclePage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected boolean isAt() {
        waitForLoadingCircleDisappear();
        waitForVisibilityOf(lstYear);
        Main.report.logInfo("Current URL address: "+driver.getCurrentUrl());
        Assert.assertEquals("Current URL address '" + driver.getCurrentUrl() + "' is not like expected 'https://auto-buy.geico.com/nb#/sale/vehicle/gskmsi/'","https://auto-buy.geico.com/nb#/sale/vehicle/gskmsi/",driver.getCurrentUrl());
        return true;
    }

    @FindBy(xpath = "//div[@class='pull-right']/button")
    private WebElement btnNEXT;

    @FindBy(xpath = "//label[@for='hasAntilockBrakes1']")
    private WebElement btnNo;

    @FindBy(xpath = "//label[@for='hasAntilockBrakes0']")
    private WebElement btnYes;

    @FindBy(id = "annualMileage")
    private WebElement lstAnnualMileage;

    @FindBy(id = "antiTheftDevice")
    private WebElement lstAntiTheftDevice;

    @FindBy(xpath = "//*[@id='vehicleMake' and not(@disabled)]")
    private WebElement lstMake;

    @FindBy(xpath = "//*[@id='vehicleModel' and not(@disabled)]")
    private WebElement lstModel;

    @FindBy(id = "vehicleYear")
    private WebElement lstYear;

    @FindAll(
            @FindBy(xpath = "//*[@id='annualMileage']/option")
    )
    private List<WebElement> dropdownAnnualMileage;

    @FindAll(
            @FindBy(xpath = "//*[@id='antiTheftDevice']/option")
    )
    private List<WebElement> dropdownAntiTheftDevice;

    @FindAll(
            @FindBy(xpath = "//*[@id='vehicleMake']/option")
    )
    private List<WebElement> dropdownMake;

    @FindAll(
            @FindBy(xpath = "//*[@id='vehicleModel']/option")
    )
    private List<WebElement> dropdownModel;

    @FindAll(
        @FindBy(xpath = "//*[@id='vehicleYear']/option")
    )
    private List<WebElement> dropdownYear;

    public By listMainlyUse(String mainlyUse) {
        return new By.ByXPath("//label/span[text()='" + mainlyUse + "']");
    }

    public By listOwnershipStatus(String status) {
        return new By.ByXPath("//label/span[text()='" + status + "']");
    }

    public void clickButtonNext() {
        click(btnNEXT, "button 'NEXT'");
    }

    public void clickButtonNo() {
        click(btnNo, "button 'No'");
    }

    public void clickButtonYes() {
        waitForVisibilityOf(btnYes);
        click(btnYes, "button 'Yes'");
    }

    public void selectAnnualMileage(String annualMileage) {
        WebElement annualMileageOption = getItemFromDropdown(lstAnnualMileage, dropdownAnnualMileage, annualMileage);
        click(annualMileageOption,"option '" + annualMileage + "' of dropdown with 'Annual Mileage'");
    }

    public void selectAntiTheftDevice(String antiTheftDevice) {
        waitForVisibilityOf(lstAntiTheftDevice);
        WebElement makeOption = getItemFromDropdown(lstAntiTheftDevice, dropdownAntiTheftDevice, antiTheftDevice);
        click(makeOption,"option '" + antiTheftDevice + "' of dropdown with anti-theft device");
    }

    public void selectMainlyUse(String mainlyUse) {
        waitForVisibilityOfElementLocatedBy(listOwnershipStatus(mainlyUse));
        click(driver.findElement(listOwnershipStatus(mainlyUse)),"option mainly use '" + mainlyUse + "'");
    }

    public void selectMake(String make) {
        waitForVisibilityOf(lstMake);
        WebElement makeOption = getItemFromDropdown(lstMake, dropdownMake, make);
        click(makeOption,"option '" + make + "' of dropdown 'Make'");
    }

    public void selectModel(String model) {
        waitForVisibilityOf(lstModel);
        WebElement modelOption = getItemFromDropdown(lstModel, dropdownModel, model);
        click(modelOption,"option '" + model + "' of dropdown 'Model'");
    }

    public void selectOwnershipStatus(String status) {
        waitForVisibilityOfElementLocatedBy(listOwnershipStatus(status));
        click(driver.findElement(listOwnershipStatus(status)),"option ownership status '" + status + "'");
    }

    public void selectYear(String year) {
        WebElement yearOption = getItemFromDropdown(lstYear, dropdownYear, year);
        click(yearOption,"option '" + year + "' of dropdown 'Year'");
    }


}
