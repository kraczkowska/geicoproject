package ai.makeitright.base;

import ai.makeitright.utilities.drivermanager.DriverConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Action extends DriverConfig {

    private Actions actions;
    private WebDriver driver;
    private WebDriverWait wait = new WebDriverWait(DriverConfig.driver, 10);
    private WebDriverWait waitShort = new WebDriverWait(DriverConfig.driver, 3);

    public Action(WebDriver driver) {
        this.driver = driver;
        actions = new Actions(this.driver);
    }

    public WebElement getItemFromDropdown(WebElement dropdown, List<WebElement> results, String option) throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(dropdown));
        Main.report.logInfo("Click dropdown");
        dropdown.click();
        Main.report.logPass("Dropdown was clicked");
        waitShort.until(ExpectedConditions.visibilityOf(results.get(0)));
        for (WebElement item : results) {
            Main.report.logInfo("#Check option '" + item.getText()+"'");
            if (item.getText().trim().equals(option)) {
                Main.report.logInfo("+The option '" + option + "' was found");
                return item;
            }
        }
        Main.report.logFail("The option '" + option + "' doesn't exist in dropdown");
        throw new InterruptedException("The option '" + option + "' doesn't exist in dropdown");
    }
}
